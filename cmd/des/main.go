package main

import (
	"feistel/internal/des/app"
	"feistel/internal/des/domain"
)

func main() {
	desService := domain.NewDesService()

	app := app.NewApp(desService)

	err := app.Init()
	if err != nil {
		panic(err)
	}

	err = app.Run()
	if err != nil {
		panic(err)
	}
}
