package main

import (
	"feistel/internal/feistel/app"
)

func main() {
	app := app.NewApp()

	err := app.Init()
	if err != nil {
		panic(err)
	}

	err = app.Run()
	if err != nil {
		panic(err)
	}
}
