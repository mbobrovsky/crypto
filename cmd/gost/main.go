package main

import (
	"feistel/internal/gost/app"
	"feistel/internal/gost/domain"
)

func main() {
	gostService := domain.NewGostService()

	app := app.NewApp(gostService)

	err := app.Init()
	if err != nil {
		panic(err)
	}

	err = app.Run()
	if err != nil {
		panic(err)
	}
}
