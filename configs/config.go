package configs

const (
	WindowsWidth  = 800
	WindowsHeight = 600
)

const (
	TextFileName          = "./text.txt"
	KeyFileName           = "./key.txt"
	EncryptedTextFileName = "./encryptedText.txt"
	DecryptedTextFileName = "./decryptedText.txt"
)
