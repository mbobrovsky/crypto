package common

func LFSR(taps []uint8, shiftRegister *uint64) byte {
	var tmp uint64 = 0
	for _, tap := range taps {
		tmp ^= *shiftRegister >> tap
	}

	*shiftRegister = (((tmp) & 0x01) << taps[0]) | (*shiftRegister >> 1)
	return byte(*shiftRegister & 0x01)
}
