package common

import (
	"testing"
)

func TestLFSR(t *testing.T) {
	taps := []uint8{2, 0}
	bits := []byte{0, 0, 1, 1, 1, 0, 1}
	states := []uint64{4, 6, 7, 3, 5, 2, 1}

	var bit byte
	var state uint64 = 1

	for i := 0; i < len(bits); i++ {
		bit = LFSR(taps, &state)
		if bit != bits[i] {
			t.Errorf("incorrect bit on possition: %d", i)
			return
		}

		if state != states[i] {
			t.Errorf("incorrect bit on possition: %d", i)
			return
		}
	}

}
