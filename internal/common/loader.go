package common

import (
	"feistel/configs"
	"os"
)

func LoadText() (string, error) {
	str, err := os.ReadFile(configs.TextFileName)
	return string(str), err
}

func LoadKey() (string, error) {
	str, err := os.ReadFile(configs.KeyFileName)
	return string(str), err
}

func LoadEncryptedText() (string, error) {
	str, err := os.ReadFile(configs.EncryptedTextFileName)
	return string(str), err
}
