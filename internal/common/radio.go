package common

const (
	EncryptAction     string = "Encrypt"
	InvestigateAction string = "Investigate"
	DecryptAction     string = "Decrypt"
)

var Actions = []string{EncryptAction, InvestigateAction, DecryptAction}

const (
	InvestigateText = "Text"
	InvestigateKey  = "Key"
)

var Investigates = []string{InvestigateText, InvestigateKey}
