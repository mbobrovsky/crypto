package common

import (
	"feistel/configs"
	"os"
)

func SaveFile(fileName string, data string) error {
	file, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}

	defer file.Close()

	file.WriteString(data)

	return nil
}

func SaveEncrypted(encryptedText string) error {
	return SaveFile(configs.EncryptedTextFileName, encryptedText)
}

func SaveDecrypted(decryptedText string) error {
	return SaveFile(configs.DecryptedTextFileName, decryptedText)
}
