package common

import (
	"fmt"
	"strconv"
	"strings"
)

const Mask28Bits uint32 = 0b1111111111111111111111111111
const Mask6Bits uint8 = 0b111111
const Mask4Bits uint8 = 0b1111

func ReverseSlice[T any](slice []T) []T {
	reversed := make([]T, 0)
	for i := len(slice) - 1; i >= 0; i-- {
		reversed = append(reversed, slice[i])
	}
	return reversed
}

func StringToUInt64(str string) uint64 {
	if len(str) > 8 {
		str = str[0:8]
	}

	var number uint64 = 0

	for i := 0; i < len(str); i++ {
		number |= uint64(str[i]) << ((7 - i) * 8)
	}

	return number
}

func UInt64ToString(number uint64) string {
	builder := strings.Builder{}

	for i := 0; i < 8; i++ {
		builder.WriteByte(byte(number >> ((7 - i) * 8)))
	}

	return builder.String()
}

func StringToUInt32(str string) uint32 {
	if len(str) > 4 {
		str = str[0:4]
	}

	var number uint32 = 0

	for i := 0; i < len(str); i++ {
		number |= uint32(str[i]) << ((3 - i) * 8)
	}

	return number
}

func Permutation(num uint64, table []uint8, numSize uint8, resSize uint8) uint64 {
	if numSize < 1 {
		return 0
	}

	var p uint64 = 0
	for i, position := range table {
		if position < 1 || position > 64 {
			continue
		}
		bit := num >> (numSize - position) & 1
		p |= bit << ((int(resSize) - 1) - i)
	}
	return p
}

func SplitBy64Bits(text string) []uint64 {
	parts := make([]uint64, 0)

	for i := 0; i < len(text); i += 8 {
		var end int

		if len(text) < 8 {
			end = len(text)
		} else {
			end = i + 8
		}

		parts = append(parts, StringToUInt64(text[i:end]))
	}

	return parts
}

func Join64Bits(parts []uint64) string {
	builder := strings.Builder{}

	for _, part := range parts {
		builder.WriteString(UInt64ToString(part))
	}

	return builder.String()
}

func SplitBy32Bits(text string) []uint32 {
	parts := make([]uint32, 0)

	for i := 0; i < len(text); i += 4 {
		var end int

		if len(text)-(i*4) < 4 {
			end = len(text)
		} else {
			end = i + 4
		}

		parts = append(parts, StringToUInt32(text[i:end]))
	}

	return parts
}

func SplitBy6Bits(number uint64) []uint8 {
	parts := make([]uint8, 0)
	for i := 0; i < 8; i++ {
		parts = append(parts, uint8(number>>((7-i)*6))&Mask6Bits)
	}
	return parts
}

func SplitBy4Bits(number uint32) []uint8 {
	parts := make([]uint8, 8)
	for i := 0; i < 8; i++ {
		parts[i] = uint8(number>>((7-i)*4)) & Mask4Bits
	}
	return parts
}

func Join4Bits(parts []uint8) uint32 {
	var number uint32 = 0
	for i := 0; i < len(parts); i++ {
		number = (number << 4) | uint32(parts[i]&Mask4Bits)
	}
	return number
}

func GetLeft32Bits(bits uint64) uint32 {
	return uint32(bits >> 32)
}

func GetRight32Bits(bits uint64) uint32 {
	return uint32(bits)
}

func JoinLeftRight32Bits(left, right uint32) uint64 {
	return uint64(left)<<32 | uint64(right)
}

func GetLeft28Bits(bits uint64) uint32 {
	return uint32(bits>>28) & Mask28Bits
}

func GetRight28Bits(bits uint64) uint32 {
	return uint32(bits) & Mask28Bits
}

func JoinLeftRight28Bits(left, right uint32) uint64 {
	return uint64(left)<<28 | uint64(right)
}

func StringToBinString(str string) string {
	bin := ""

	for _, c := range str {
		bin = fmt.Sprintf("%s%.8b", bin, c)
	}

	return bin
}

func CompareBits(num1 uint64, num2 uint64) uint8 {
	var changes uint8 = 0

	for i := 0; i < 64; i++ {
		if ((num1 >> i) & 0x01) != ((num2 >> i) & 0x01) {
			changes++
		}
	}

	return changes
}

func StringToPrintHexString(str string) string {
	hexStr := make([]string, len(str), len(str))

	for i := 0; i < len(str); i++ {
		hexStr[i] = fmt.Sprintf("%x", str[i])
	}

	return strings.Join(hexStr, " ")
}

func StringFromPrintHexString(hexStr string) (string, error) {
	builder := strings.Builder{}
	parts := strings.Split(hexStr, " ")

	for _, hex := range parts {
		value, err := strconv.ParseUint(hex, 16, 8)

		if err != nil {
			return "", err
		}

		builder.WriteByte(byte(value))
	}

	return builder.String(), nil
}

func StringToPrintBinString(str string) string {
	hex := make([]string, len(str), len(str))

	for i := 0; i < len(str); i++ {
		hex[i] = fmt.Sprintf("%.8b", str[i])
	}

	return strings.Join(hex, " ")
}

func StringFromPrintBinString(binStr string) (string, error) {
	builder := strings.Builder{}
	parts := strings.Split(binStr, " ")

	for _, bin := range parts {
		value, err := strconv.ParseUint(bin, 2, 8)

		if err != nil {
			return "", err
		}

		builder.WriteByte(byte(value))
	}

	return builder.String(), nil
}
