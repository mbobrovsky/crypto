package app

type DesService interface {
	Encrypt(text string, keyStr string) string
	Decrypt(text string, keyStr string) string
	Investigate(text string, key string, investigate string, position int) []uint8
}
