package app

const (
	EncryptAction     string = "Encrypt"
	InvestigateAction string = "Investigate"
	DecryptAction     string = "Decrypt"
)

var Actions = []string{EncryptAction, InvestigateAction, DecryptAction}
