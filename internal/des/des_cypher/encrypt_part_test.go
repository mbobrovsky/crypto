package des_cypher

import (
	"feistel/internal/common"
	"testing"
)

func TestEncryptDecrypt(t *testing.T) {
	text := "ABCDEFGH"
	keyStr := "qwerty"

	parts := common.SplitBy64Bits(text)
	key := common.StringToUInt64(keyStr)

	roundKeys := GenerateRoundKeys(key)
	reversedRoundKeys := common.ReverseSlice(GenerateRoundKeys(key))

	encryptedParts := make([]uint64, len(parts))
	for i := 0; i < len(parts); i++ {
		encryptedParts[i] = EncryptPart(parts[i], roundKeys)
	}

	//encrypted := common.Join64Bits(encryptedParts)
	//encryptedParts = common.SplitBy64Bits(encrypted)

	decryptedParts := make([]uint64, len(encryptedParts))
	for i := 0; i < len(parts); i++ {
		decryptedParts[i] = EncryptPart(encryptedParts[i], reversedRoundKeys)
	}

	decrypted := common.Join64Bits(decryptedParts)

	if text != decrypted {
		t.Errorf("decrypted text %s not equal plain text %s", decrypted, text)
	}
}

func TestEncryptDecryptPart(t *testing.T) {
	var part uint64 = 4702394921427289928
	var key uint64 = 8176115188815101952

	roundKeys := GenerateRoundKeys(key)
	reversedRoundKeys := common.ReverseSlice(GenerateRoundKeys(key))

	encryptedPart := EncryptPart(part, roundKeys)
	decryptedPart := EncryptPart(encryptedPart, reversedRoundKeys)

	if part != decryptedPart {
		t.Errorf("part %d not equal decrypted part %d", part, decryptedPart)
	}
}
