package domain

import (
	"feistel/internal/common"
	"feistel/internal/des/des_cypher"
)

type DesService struct{}

func (s *DesService) Encrypt(text string, keyStr string) string {
	return des_cypher.Encrypt(text, keyStr, false)
}

func (s *DesService) Decrypt(text string, keyStr string) string {
	return des_cypher.Encrypt(text, keyStr, true)
}

func (s *DesService) Investigate(text string, keyStr string, investigate string, position int) []uint8 {
	parts := common.SplitBy64Bits(text)
	if len(parts) == 0 {
		return nil
	}

	part := parts[0]
	changedPart := part

	key := common.StringToUInt64(keyStr)

	var roundKeysChanged []uint64

	if investigate == common.InvestigateText {
		roundKeysChanged = des_cypher.GenerateRoundKeys(changedPart ^ uint64(1)<<(63-position))
	} else if investigate == common.InvestigateKey {
		roundKeysChanged = des_cypher.GenerateRoundKeys(key ^ uint64(1)<<(63-position))
	}

	_, roundsOriginal := des_cypher.EncryptPart(part, des_cypher.GenerateRoundKeys(key))
	_, roundsChanged := des_cypher.EncryptPart(changedPart, roundKeysChanged)

	changes := make([]uint8, len(roundsOriginal))

	for i := 0; i < len(roundsOriginal); i++ {
		changes[i] = common.CompareBits(roundsOriginal[i], roundsChanged[i])
	}

	return changes
}

func NewDesService() *DesService {
	return &DesService{}
}
