package app

const (
	SubKeyFromKey       = "SubKey From Key"
	SubKeyFromScrambler = "SubKey From Scrambler"
)

var SubKeys = []string{SubKeyFromKey, SubKeyFromScrambler}

const (
	SingleFunction    = "F(Vi) = Vi"
	ScramblerFunction = "F(Vi, X) = S(X) ^ Vi"
)

var Functions = []string{SingleFunction, ScramblerFunction}
