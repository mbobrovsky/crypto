package feistel_cypher

import (
	"feistel/internal/common"
	"feistel/internal/feistel/app"
)

const rounds = 16
const scrambleInitialValue uint64 = 3

var taps = []uint8{16, 14, 13, 11}

func Encrypt(text string, key string, subKeyType string, funcType string) string {
	if len(text) == 0 || len(key) == 0 {
		return text
	}

	parts := common.SplitBy64Bits(text)
	encryptedParts := make([]uint64, 0)

	for _, part := range parts {
		encryptedParts = append(encryptedParts, encryptPart(part, key, subKeyType, funcType))
	}

	encryptedText := common.Join64Bits(encryptedParts)

	return encryptedText
}

func Investigate(text string, key string, subKeyType string, funcType string, investigate string, position int) []uint8 {
	if len(text) == 0 || len(key) == 0 {
		return nil
	}

	changes := make([]uint8, rounds)

	parts := common.SplitBy64Bits(text)
	part := parts[0]

	changedPart := part
	changedKey := key

	if investigate == common.InvestigateText {
		changedPart ^= 1 << (63 - position)
	} else {
		bytes := []byte(changedKey)
		bytes[position/8] ^= 1 << (8 - position/8)
		changedKey = string(bytes)
	}

	left := common.GetLeft32Bits(part)
	right := common.GetRight32Bits(part)

	changedLeft := common.GetLeft32Bits(changedPart)
	changedRight := common.GetRight32Bits(changedPart)

	for i := 0; i < rounds; i++ {
		encryptRound(&changedLeft, &changedRight, changedKey, subKeyType, funcType, i)

		changes[i] = common.CompareBits(
			common.JoinLeftRight32Bits(left, right),
			common.JoinLeftRight32Bits(changedLeft, changedRight),
		)
	}

	return changes
}

func encryptPart(part uint64, key string, subKeyType string, funcType string) uint64 {
	left := common.GetLeft32Bits(part)
	right := common.GetRight32Bits(part)

	for i := 0; i < rounds; i++ {
		encryptRound(&left, &right, key, subKeyType, funcType, i)
	}

	return common.JoinLeftRight32Bits(left, right)
}

func encryptRound(left *uint32, right *uint32, key string, subKeyType, funcType string, index int) {
	subKey := computeSubKeyByType(key, subKeyType, index)
	var x *uint32

	if funcType != app.SingleFunction {
		x = left
	}

	*left, *right = function(subKey, x)^*right, *left
}

func computeSubKeyByType(key string, subKeyType string, index int) uint32 {
	var subKey uint32

	if subKeyType == app.SubKeyFromKey {
		subKey = computeSubKey(key, index, 32)
	} else if subKeyType == app.SubKeyFromScrambler {
		shiftRegister := uint64(computeSubKey(key, index, 8))
		for i := 0; i < 32; i++ {
			subKey = subKey<<1 | uint32(common.LFSR(taps, &shiftRegister))
		}
	}

	return subKey
}

func computeSubKey(key string, index int, size int) uint32 {
	var subKey uint32

	binKey := common.StringToBinString(key)

	if len(binKey) == 0 {
		return subKey
	}

	currentIndex := index

	for currentIndex >= len(binKey) {
		currentIndex -= currentIndex / len(binKey) * len(binKey)
	}

	for i := 0; i < size; i++ {
		subKey = (subKey << 1) | uint32(binKey[currentIndex]-'0')

		if currentIndex < len(binKey)-1 {
			currentIndex++
		} else {
			currentIndex = 0
		}
	}

	return subKey

}

func function(subKey uint32, x *uint32) uint32 {
	if x == nil {
		return subKey
	} else {
		var sx uint32
		shiftRegister := scrambleInitialValue
		for i := 0; i < 32; i++ {
			sx = sx<<1 | uint32(common.LFSR(taps, &shiftRegister))
		}

		return sx ^ subKey
	}
}
