package app

import (
	"bytes"
	"feistel/configs"
	"feistel/internal/common"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
	"github.com/wcharczuk/go-chart"
	"log"
	"strconv"
)

const windowTitle = "GOST 28147-89"

type App struct {
	gostService GostService

	app            fyne.App
	window         fyne.Window
	container      *fyne.Container
	chartContainer *fyne.Container

	actionRadioGroup *widget.RadioGroup

	textEntry      *widget.Entry
	textInHexEntry *widget.Entry
	textInBinEntry *widget.Entry

	encryptedTextEntry      *widget.Entry
	encryptedTextInHexEntry *widget.Entry
	encryptedTextInBinEntry *widget.Entry

	keyEntry      *widget.Entry
	keyInHexEntry *widget.Entry
	keyInBinEntry *widget.Entry

	resultEntry      *widget.Entry
	resultInHexEntry *widget.Entry
	resultInBinEntry *widget.Entry

	investigateRadioGroup *widget.RadioGroup

	positionEntry *common.IntEntry

	formText          *widget.Form
	formEncryptedText *widget.Form
	formKey           *widget.Form
	formResult        *widget.Form
	formInvestigate   *widget.Form

	encryptBtn     *widget.Button
	investigateBtn *widget.Button
	decryptBtn     *widget.Button

	chartImage *canvas.Image
}

func (a *App) Init() error {
	a.app = app.New()
	a.window = a.app.NewWindow(windowTitle)
	a.container = container.NewVBox()
	a.chartContainer = container.NewVBox()

	a.actionRadioGroup = widget.NewRadioGroup(Actions, a.onChangeAction)
	a.actionRadioGroup.Horizontal = true

	a.textEntry = widget.NewEntry()
	a.textEntry.OnChanged = a.onChangeText

	a.textInHexEntry = widget.NewEntry()
	a.textInHexEntry.OnChanged = a.onChangeTextInHex

	a.textInBinEntry = widget.NewEntry()
	a.textInBinEntry.OnChanged = a.onChangeTextInBin

	a.encryptedTextEntry = widget.NewEntry()
	a.encryptedTextEntry.OnChanged = a.onChangeEncryptedText

	a.encryptedTextInHexEntry = widget.NewEntry()
	a.encryptedTextInHexEntry.OnChanged = a.onChangeEncryptedTextInHex

	a.encryptedTextInBinEntry = widget.NewEntry()
	a.encryptedTextInBinEntry.OnChanged = a.onChangeEncryptedTextInBin

	a.keyEntry = widget.NewEntry()
	a.keyEntry.OnChanged = a.onChangeKey

	a.keyInHexEntry = widget.NewEntry()
	a.keyInHexEntry.OnChanged = a.onChangeKeyInHex

	a.keyInBinEntry = widget.NewEntry()
	a.keyInBinEntry.OnChanged = a.onChangeKeyInBin

	a.investigateRadioGroup = widget.NewRadioGroup(common.Investigates, func(s string) {})
	a.investigateRadioGroup.Horizontal = true
	a.investigateRadioGroup.SetSelected(common.InvestigateText)

	a.positionEntry = common.NewIntEntry()
	a.positionEntry.SetText("0")
	a.positionEntry.OnChanged = a.onChangePositionEntry

	a.resultEntry = widget.NewEntry()
	a.resultEntry.OnChanged = a.onChangeResult

	a.resultInHexEntry = widget.NewEntry()
	a.resultInHexEntry.OnChanged = a.onChangeResultInHex

	a.resultInBinEntry = widget.NewEntry()
	a.resultInBinEntry.OnChanged = a.onChangeResultInBin

	a.encryptBtn = widget.NewButton("Encrypt", a.onClickEncrypt)
	a.investigateBtn = widget.NewButton("Investigate", a.onClickInvestigate)
	a.decryptBtn = widget.NewButton("Decrypt", a.onClickDecrypt)

	a.chartImage = &canvas.Image{}
	a.chartImage.FillMode = canvas.ImageFillOriginal

	a.window.SetContent(container.NewVScroll(a.container))
	a.window.Resize(fyne.NewSize(configs.WindowsWidth, configs.WindowsHeight))

	a.onChangeAction(EncryptAction)

	return nil
}

func (a *App) initEncrypt() {
	a.actionRadioGroup.SetSelected(EncryptAction)

	text, _ := common.LoadText()
	key, _ := common.LoadKey()

	a.textEntry.SetText(text)
	a.keyEntry.SetText(key)

	a.formText = widget.NewForm(
		widget.NewFormItem("Text:", a.textEntry),
		widget.NewFormItem("Text in Hex:", a.textInHexEntry),
		widget.NewFormItem("Text in Bin:", a.textInBinEntry),
	)

	a.formKey = widget.NewForm(
		widget.NewFormItem("Key:", a.keyEntry),
		widget.NewFormItem("Key in Hex:", a.keyInHexEntry),
		widget.NewFormItem("Key in Bin:", a.keyInBinEntry),
	)

	a.formResult = widget.NewForm(
		widget.NewFormItem("Result:", a.resultEntry),
		widget.NewFormItem("Result in Hex:", a.resultInHexEntry),
		widget.NewFormItem("Result in Bin", a.resultInBinEntry),
	)

	a.formResult.Hide()

	a.container.RemoveAll()
	a.container.Add(a.actionRadioGroup)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formText)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formKey)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.encryptBtn)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formResult)
}

func (a *App) initInvestigate() {
	a.actionRadioGroup.SetSelected(InvestigateAction)

	text, _ := common.LoadText()
	key, _ := common.LoadKey()

	a.textEntry.SetText(text)
	a.keyEntry.SetText(key)

	a.formText = widget.NewForm(
		widget.NewFormItem("Text:", a.textEntry),
		widget.NewFormItem("Text in Hex:", a.textInHexEntry),
		widget.NewFormItem("Text in Bin:", a.textInBinEntry),
	)

	a.formKey = widget.NewForm(
		widget.NewFormItem("Key:", a.keyEntry),
		widget.NewFormItem("Key in Hex:", a.keyInHexEntry),
		widget.NewFormItem("Key in Bin:", a.keyInBinEntry),
	)

	a.formInvestigate = widget.NewForm(
		widget.NewFormItem("Investigate: ", a.investigateRadioGroup),
		widget.NewFormItem("Position: ", a.positionEntry),
	)

	a.container.RemoveAll()
	a.container.Add(a.actionRadioGroup)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formText)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formKey)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formInvestigate)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.investigateBtn)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.chartContainer)
}

func (a *App) initDecrypt() {
	a.actionRadioGroup.SetSelected(DecryptAction)

	encryptedText, _ := common.LoadEncryptedText()
	key, _ := common.LoadKey()

	a.encryptedTextEntry.SetText(encryptedText)
	a.keyEntry.SetText(key)

	a.formEncryptedText = widget.NewForm(
		widget.NewFormItem("Encrypted text:", a.encryptedTextEntry),
		widget.NewFormItem("Encrypted text in Hex:", a.encryptedTextInHexEntry),
		widget.NewFormItem("Encrypted text in Bin:", a.encryptedTextInBinEntry),
	)

	a.formKey = widget.NewForm(
		widget.NewFormItem("Key:", a.keyEntry),
		widget.NewFormItem("Key in Hex:", a.keyInHexEntry),
		widget.NewFormItem("Key in Bin:", a.keyInBinEntry),
	)

	a.formResult = widget.NewForm(
		widget.NewFormItem("Result:", a.resultEntry),
		widget.NewFormItem("Result in Hex:", a.resultInHexEntry),
		widget.NewFormItem("Result in Bin", a.resultInBinEntry),
	)

	a.formResult.Hide()

	a.container.RemoveAll()
	a.container.Add(a.actionRadioGroup)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formEncryptedText)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formKey)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.decryptBtn)
	a.container.Add(widget.NewSeparator())
	a.container.Add(a.formResult)
}

func (a *App) onChangeAction(action string) {
	switch action {
	case EncryptAction:
		a.initEncrypt()
	case InvestigateAction:
		a.initInvestigate()
	case DecryptAction:
		a.initDecrypt()
	default:
		return
	}
}

func (a *App) onChangeText(newText string) {
	a.textInHexEntry.SetText(common.StringToPrintHexString(newText))
	a.textInBinEntry.SetText(common.StringToPrintBinString(newText))
}

func (a *App) onChangeTextInHex(hexText string) {
	text, err := common.StringFromPrintHexString(hexText)
	if err != nil {
		a.textInHexEntry.SetText(common.StringToPrintHexString(a.textEntry.Text))
		return
	}
	a.textEntry.SetText(text)
}

func (a *App) onChangeTextInBin(binText string) {
	text, err := common.StringFromPrintBinString(binText)
	if err != nil {
		a.textInBinEntry.SetText(common.StringToPrintBinString(a.textEntry.Text))
		return
	}
	a.textEntry.SetText(text)
}

func (a *App) onChangeEncryptedText(newEncryptedText string) {
	a.encryptedTextInHexEntry.SetText(common.StringToPrintHexString(newEncryptedText))
	a.encryptedTextInBinEntry.SetText(common.StringToPrintBinString(newEncryptedText))
}

func (a *App) onChangeEncryptedTextInHex(hexEncryptedText string) {
	encryptedText, err := common.StringFromPrintHexString(hexEncryptedText)
	if err != nil {
		a.encryptedTextInHexEntry.SetText(common.StringToPrintHexString(a.encryptedTextEntry.Text))
		return
	}
	a.encryptedTextEntry.SetText(encryptedText)
}

func (a *App) onChangeEncryptedTextInBin(binEncryptedText string) {
	encryptedText, err := common.StringFromPrintBinString(binEncryptedText)
	if err != nil {
		a.encryptedTextInHexEntry.SetText(common.StringToPrintHexString(a.encryptedTextEntry.Text))
		return
	}
	a.encryptedTextEntry.SetText(encryptedText)
}

func (a *App) onChangeKey(newKey string) {
	a.keyInHexEntry.SetText(common.StringToPrintHexString(newKey))
	a.keyInBinEntry.SetText(common.StringToPrintBinString(newKey))
}

func (a *App) onChangeKeyInHex(hexKey string) {
	key, err := common.StringFromPrintHexString(hexKey)
	if err != nil {
		a.keyInHexEntry.SetText(common.StringToPrintHexString(a.keyEntry.Text))
		return
	}
	a.keyEntry.SetText(key)
}

func (a *App) onChangeKeyInBin(binKey string) {
	key, err := common.StringFromPrintBinString(binKey)
	if err != nil {
		a.keyInBinEntry.SetText(common.StringToPrintBinString(a.keyEntry.Text))
		return
	}
	a.keyEntry.SetText(key)
}

func (a *App) onChangeResult(newKey string) {
	a.resultInHexEntry.SetText(common.StringToPrintHexString(newKey))
	a.resultInBinEntry.SetText(common.StringToPrintBinString(newKey))
}

func (a *App) onChangeResultInHex(hexResult string) {
	result, err := common.StringFromPrintHexString(hexResult)
	if err != nil {
		a.resultInHexEntry.SetText(common.StringToPrintHexString(a.resultEntry.Text))
		return
	}
	a.resultEntry.SetText(result)
}

func (a *App) onChangeResultInBin(binResult string) {
	result, err := common.StringFromPrintBinString(binResult)
	if err != nil {
		a.resultInBinEntry.SetText(common.StringToPrintBinString(a.resultEntry.Text))
		return
	}
	a.resultEntry.SetText(result)
}

func (a *App) onChangePositionEntry(positionStr string) {
	position, _ := strconv.Atoi(positionStr)
	if position > 63 {
		a.positionEntry.SetText("63")
	}
}

func (a *App) onClickEncrypt() {
	encryptedText := a.gostService.Encrypt(a.textEntry.Text, a.keyEntry.Text)
	common.SaveEncrypted(encryptedText)
	a.resultEntry.SetText(encryptedText)
	a.formResult.Show()
}

func (a *App) onClickInvestigate() {
	position, _ := strconv.Atoi(a.positionEntry.Text)
	changes := a.gostService.Investigate(a.textEntry.Text, a.keyEntry.Text, a.investigateRadioGroup.Selected, position)

	if len(changes) == 0 {
		return
	}

	xValues := make([]float64, len(changes))
	yValues := make([]float64, len(changes))

	for i, change := range changes {
		xValues[i] = float64(i + 1)
		yValues[i] = float64(change)
	}

	graph := chart.Chart{
		Title:      "Avalanche",
		TitleStyle: chart.StyleShow(),
		Width:      configs.WindowsWidth - 10,
		XAxis: chart.XAxis{
			Name: "Round",
			NameStyle: chart.Style{
				Show: true,
			},
			Style: chart.Style{
				Show: true,
			},
		},
		YAxis: chart.YAxis{
			Name: "Changes",
			NameStyle: chart.Style{
				Show: true,
			},
			Style: chart.Style{
				Show: true,
			},
		},
		Series: []chart.Series{
			chart.ContinuousSeries{
				XValues: xValues,
				YValues: yValues,
			},
		},
	}

	buffer := bytes.NewBuffer([]byte{})
	err := graph.Render(chart.PNG, buffer)
	if err != nil {
		log.Println(err)
		return
	}

	a.chartContainer.RemoveAll()
	a.chartImage = canvas.NewImageFromReader(bytes.NewReader(buffer.Bytes()), "Avalanche")
	a.chartContainer.Add(a.chartImage)
	a.chartImage.FillMode = canvas.ImageFillOriginal
}

func (a *App) onClickDecrypt() {
	decryptedText := a.gostService.Decrypt(a.encryptedTextEntry.Text, a.keyEntry.Text)
	common.SaveDecrypted(decryptedText)
	a.resultEntry.SetText(decryptedText)
	a.formResult.Show()
}

func (a *App) Run() error {
	if a.app == nil {
		return fmt.Errorf("app not initialized")
	}

	a.window.ShowAndRun()

	return nil
}

func NewApp(gostService GostService) *App {
	return &App{
		gostService: gostService,
	}
}
