package domain

import (
	"feistel/internal/common"
	"feistel/internal/gost/gost_cypher"
)

type GostService struct{}

func (s *GostService) Encrypt(text string, keyStr string) string {
	return gost_cypher.Encrypt(text, keyStr, false)
}

func (s *GostService) Decrypt(text string, keyStr string) string {
	return gost_cypher.Encrypt(text, keyStr, true)
}

func (s *GostService) Investigate(text string, keyStr string, investigate string, position int) []uint8 {
	parts := common.SplitBy64Bits(text)
	if len(parts) == 0 {
		return nil
	}

	part := parts[0]
	changedPart := part

	var roundKeysChanged []uint32

	if investigate == common.InvestigateText {
		roundKeysChanged = gost_cypher.GenerateRoundKeys(keyStr)
	} else if investigate == common.InvestigateKey {
		roundKeysChanged = gost_cypher.GenerateRoundKeys(keyStr)
	}

	_, roundsOriginal := gost_cypher.EncryptPart(part, gost_cypher.GenerateRoundKeys(keyStr))
	_, roundsChanged := gost_cypher.EncryptPart(changedPart, roundKeysChanged)

	changes := make([]uint8, len(roundsOriginal))

	for i := 0; i < len(roundsOriginal); i++ {
		changes[i] = common.CompareBits(roundsOriginal[i], roundsChanged[i])
	}

	return changes
}

func NewGostService() *GostService {
	return &GostService{}
}
