package gost_cypher

import "feistel/internal/common"

func Encrypt(text string, keyStr string, decrypt bool) string {
	parts := common.SplitBy64Bits(text)

	roundKeys := GenerateRoundKeys(keyStr)
	if decrypt {
		roundKeys = common.ReverseSlice(roundKeys)
	}

	encryptedParts := make([]uint64, 0)
	for _, part := range parts {
		encryptedPart, _ := EncryptPart(part, roundKeys)
		encryptedParts = append(encryptedParts, encryptedPart)
	}

	return common.Join64Bits(encryptedParts)
}
