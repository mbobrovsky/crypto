package gost_cypher

import (
	"feistel/internal/common"
)

const Rounds = 32

func EncryptPart(part uint64, roundKeys []uint32) (uint64, []uint64) {
	left := common.GetLeft32Bits(part)
	right := common.GetRight32Bits(part)

	rounds := make([]uint64, Rounds)

	for i := 0; i < Rounds; i++ {
		left = left ^ Function(right, roundKeys[i])
		if i != Rounds-1 {
			left, right = right, left
		}
		rounds[i] = common.JoinLeftRight32Bits(left, right)
	}

	return common.JoinLeftRight32Bits(left, right), rounds
}
