package gost_cypher

import (
	"testing"
)

func TestEncryptDecrypt(t *testing.T) {
	text := "ABCDEFGH"
	keyStr := "12345678901234567890123456789012"

	encrypted := Encrypt(text, keyStr, false)
	decrypted := Encrypt(encrypted, keyStr, true)

	if text != decrypted {
		t.Errorf("decrypted text %s not equal plain text %s", decrypted, text)
	}
}

func TestEncryptDecryptPart(t *testing.T) {
	//var part uint64 = 4702394921427289928
	//var key uint64 = 8176115188815101952
	//
	//roundKeys := GenerateRoundKeys(key)
	//reversedRoundKeys := common.ReverseSlice(GenerateRoundKeys(key))
	//
	//encryptedPart := EncryptPart(part, roundKeys)
	//decryptedPart := EncryptPart(encryptedPart, reversedRoundKeys)
	//
	//if part != decryptedPart {
	//	t.Errorf("part %d not equal decrypted part %d", part, decryptedPart)
	//}
}
