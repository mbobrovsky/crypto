package gost_cypher

import "feistel/internal/common"

var roundKeyTable = []int{
	1, 2, 3, 4, 5, 6, 7, 8,
	1, 2, 3, 4, 5, 6, 7, 8,
	1, 2, 3, 4, 5, 6, 7, 8,
	8, 7, 6, 5, 4, 3, 2, 1,
}

func GenerateRoundKeys(key string) []uint32 {
	if len(key) > 32 {
		key = key[0:32]
	}

	roundKeys := make([]uint32, 32)

	parts := common.SplitBy32Bits(key)

	for i := 0; i < len(roundKeyTable); i++ {
		if len(parts) > roundKeyTable[i]-1 {
			roundKeys[i] = parts[roundKeyTable[i]-1]
		}
	}

	return roundKeys
}
